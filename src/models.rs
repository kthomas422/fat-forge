use num_cpus;
use sqlx::{
    sqlite::{SqliteConnectOptions, SqliteJournalMode, SqlitePoolOptions, SqliteSynchronous},
    SqlitePool,
};
use std::sync::Arc;
use std::time::Duration;
use tokio::sync::Mutex;

use crate::roles;

// https://github.com/launchbadge/sqlx/blob/main/examples/sqlite/todos/src/main.rs

#[derive(Debug)]
pub struct Database {
    reader: sqlx::SqlitePool,
    writer: Arc<Mutex<SqlitePool>>,
}

impl Database {
    pub async fn new(db_path: String) -> Result<Self, sqlx::Error> {
        let max_connections: u32 = match num_cpus::get().try_into() {
            Ok(value) => value,
            Err(_) => 4,
        };

        let read_options = SqliteConnectOptions::new()
            .read_only(true)
            .create_if_missing(false)
            .journal_mode(SqliteJournalMode::Wal)
            .busy_timeout(Duration::new(5, 0)) // 5s
            .synchronous(SqliteSynchronous::Normal)
            .pragma("cache_size", "1000000000")
            .pragma("foreign_keys", "true")
            .pragma("temp_store", "memory")
            .filename(db_path.clone());

        // Create a read-only connection pool
        let read_pool = SqlitePoolOptions::new()
            .max_connections(max_connections)
            .connect_with(read_options)
            .await?;

        let write_options = SqliteConnectOptions::new()
            .create_if_missing(true)
            .journal_mode(SqliteJournalMode::Wal)
            .busy_timeout(Duration::new(5, 0)) // 5s
            .synchronous(SqliteSynchronous::Normal)
            .pragma("cache_size", "1000000000")
            .pragma("foreign_keys", "true")
            .pragma("temp_store", "memory")
            .filename(db_path);

        // Create a writable connection pool of 1
        let write_pool = SqlitePoolOptions::new()
            .max_connections(1)
            .connect_with(write_options)
            .await?;

        Ok(Database {
            reader: read_pool,
            writer: Arc::new(Mutex::new(write_pool)), // the writer will be guarded by a mutex
        })
    }
    pub async fn close(self) {
        self.reader.close().await;
        self.writer.lock().await.close().await;
    }
}

// all db id's are auto incrementing ints since its performant and not a distributed application

#[derive(Debug)]
struct FatGroup {
    id: i32,
    parent_id_key: i32, // Groups can be nested
    name: String,       // Group Name
    fats: Vec<Fat>,
}

#[derive(Debug)]
struct Fat {
    id: i32,
    fat_group_key: i32, // foreign key to FatGroup
    name: String,       // FAT Name
    setup: String,      // Instructions to the user on setting up this test
    summary: String,    // Short Summary of what is being tested
    actions: Vec<TestAction>,
    runs: Vec<TestRun>,
}

#[derive(Debug)]
struct TestRun {
    id: i32,
    fat_key: i32,              // foreign key to Fat
    date: i32,                 // unix time for the date the test was done
    tester_name: String,       // name of the person performing the FAT
    environment_notes: String, // any relevant notes about user environment
    results: Vec<TestCaseResult>,
}

#[derive(Debug)]
struct TestAction {
    id: i32,
    fat_key: i32,  // foreign key to Fat
    number: i32,   // which action/step it is in the test
    steps: String, // the text description or steps to take
    cases: Vec<TestCase>,
    attachments: Vec<Attachment>,
}

// This is separate from a TestAction because one action can have multiple
// results to check
#[derive(Debug)]
struct TestCase {
    id: i32,
    test_action_key: i32, // foreign key to TestAction
    number: i32,          // the test case number
    description: String,  // the test case criteria for passing
    results: Vec<TestCaseResult>,
}

#[derive(Debug)]
struct TestCaseResult {
    id: i32,
    test_case_key: i32, // foreign key to the TestCase
    test_run_key: i32,  // foreign key to the test run
    passed: bool,       // test pass or fail
    notes: String,      // tester provided notes on test case
    attachments: Vec<Attachment>,
}

#[derive(Debug)]
struct Attachment {
    id: i32,
    file_name: String,
}

#[derive(Debug)]
struct User {
    id: i32,
    name: String,
    password: String, // argon2 hash
    roles: Vec<roles::Role>,
}
