mod models;
mod roles;

use askama_axum::Template;
use axum::{http::StatusCode, response::Html, routing::get, Router};
use config::{Config, ConfigError};
use log::*;
use serde::Deserialize;
use simplelog::*;

#[derive(Debug, Deserialize)]
struct Cfg {
    debug: bool,             // spam logs or not
    db_path: String,         // where the SQLite database file is
    attachment_path: String, // where to store the test attachment files
}

impl Cfg {
    fn new() -> Result<Self, ConfigError> {
        let settings = Config::builder()
            .add_source(config::File::with_name("./fat_forge.ron"))
            .add_source(config::Environment::with_prefix("FAT_FORGE"))
            .set_default("debug", false)?
            .set_default("db_path", "./fat_forge.db")?
            .set_default("attachment_path", "./attachments")?
            .build();

        // Try to deserialize into the Cfg struct
        settings?.try_deserialize()
    }
}

#[tokio::main]
async fn main() {
    // init config
    let cfg = match Cfg::new() {
        Ok(value) => value,
        Err(e) => {
            println!("failed reading config: {}", e);
            return;
        }
    };

    let log_level = if cfg.debug {
        LevelFilter::Debug
    } else {
        LevelFilter::Info
    };

    if let Err(e) = TermLogger::init(
        log_level,
        simplelog::ConfigBuilder::new()
            .set_time_format_rfc3339()
            .build(),
        TerminalMode::Stdout,
        ColorChoice::Auto,
    ) {
        println!("failed initializing logger: {}", e);
        return;
    }
    debug!("logger initialized");

    // init database connection
    let db_pool = match models::Database::new(cfg.db_path).await {
        Ok(value) => value,
        Err(e) => {
            error!("failed opening database: {}", e);
            return;
        }
    };
    debug!("database pools established");

    let app = Router::new().route("/", get(root_handler));

    // cleanup
    db_pool.close().await;
    debug!("database pools closed");
}

#[derive(Template)]
#[template(path = "hello.html")]
struct HelloTemplate {
    name: String,
}

async fn root_handler() -> Result<Html<String>, (StatusCode, String)> {
    let template = HelloTemplate {
        name: "Axum User".to_string(),
    };
    template
        .render()
        .map(Html)
        .map_err(|e| (StatusCode::INTERNAL_SERVER_ERROR < e.to_string()))
}
