#[derive(Debug)]
pub enum Role {
    CreateTests,
    RunTests,
    ManageUsers,
}

impl Role {
    const CREATE_TESTS_ID: i32 = 1;
    const RUN_TESTS_ID: i32 = 2;
    const MANAGE_USERS_ID: i32 = 3;

    pub fn id(&self) -> i32 {
        match *self {
            Role::CreateTests => Role::CREATE_TESTS_ID,
            Role::RunTests => Role::RUN_TESTS_ID,
            Role::ManageUsers => Role::MANAGE_USERS_ID,
        }
    }
}
