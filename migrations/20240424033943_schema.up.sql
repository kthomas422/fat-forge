CREATE TABLE IF NOT EXISTS FatGroup (
    id INTEGER PRIMARY KEY,
    parent_id_key INTEGER,
    name TEXT NOT NULL,
    FOREIGN KEY (parent_id_key) REFERENCES FatGroup(id)
) STRICT;

CREATE TABLE IF NOT EXISTS Fat (
    id INTEGER PRIMARY KEY,
    fat_group_key INTEGER NOT NULL,
    name TEXT NOT NULL,
    setup TEXT NOT NULL,
    summary TEXT NOT NULL,
    FOREIGN KEY (fat_group_key) REFERENCES FatGroup(id)
) STRICT;

CREATE TABLE IF NOT EXISTS TestRun (
    id INTEGER PRIMARY KEY,
    fat_key INTEGER NOT NULL,
    date INTEGER NOT NULL,
    tester_name TEXT NOT NULL,
    environment_notes TEXT NOT NULL,
    FOREIGN KEY (fat_key) REFERENCES Fat(id)
) STRICT;

CREATE TABLE IF NOT EXISTS TestAction (
    id INTEGER PRIMARY KEY,
    fat_key INTEGER NOT NULL,
    number INTEGER NOT NULL,
    steps TEXT NOT NULL,
    FOREIGN KEY (fat_key) REFERENCES Fat(id)
) STRICT;

CREATE TABLE IF NOT EXISTS TestCase (
    id INTEGER PRIMARY KEY,
    test_action_key INTEGER NOT NULL,
    number INTEGER NOT NULL,
    description TEXT NOT NULL,
    FOREIGN KEY (test_action_key) REFERENCES TestAction(id)
) STRICT;

CREATE TABLE IF NOT EXISTS TestCaseResult (
    id INTEGER PRIMARY KEY,
    test_case_key INTEGER NOT NULL,
    test_run_key INTEGER NOT NULL,
    passed INTEGER NOT NULL, -- boolean
    notes TEXT NOT NULL,
    FOREIGN KEY (test_case_key) REFERENCES TestCase(id),
    FOREIGN KEY (test_run_key) REFERENCES TestRun(id)
);

CREATE TABLE IF NOT EXISTS Attachment (
    id INTEGER PRIMARY KEY,
    file_name TEXT NOT NULL
) STRICT;

CREATE TABLE IF NOT EXISTS ResultAttachment (
    attachment_id: int,
    result_id: int,
    FOREIGN KEY (attachment_id) REFRENCES Attachment(id),
    FOREIGN KEY (result_id) REFRENCES TestCaseResult(id),
    PRIMARY KEY (attachment_id, result_id)
) STRICT;

CREATE TABLE IF NOT EXISTS ActionAttachment (
    attachment_id: int,
    action_id: int,
    FOREIGN KEY (attachment_id) REFRENCES Attachment(id),
    FOREIGN KEY (action_id) REFRENCES TestAction(id),
    PRIMARY KEY (attachment_id, action_id)
) STRICT;

CREATE TABLE IF NOT EXISTS User (
    id INTEGER PRIMARY KEY,
    name TEXT NOT NULL,
) STRICT;

CREATE TABLE IF NOT EXISTS UserRole (
    user_id: int,
    role_id: int,
    FOREIGN KEY (user_id) REFERENCES User(id),
    PRIMARY KEY (user_id, role_id),
) STRICT;
