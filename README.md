# FAT Forge

Factory Acceptance Testing Forge

A web app that allows teams to create and collaborate on FAT tests

## Motivation

Working on a small team of 7-9 developers we often would have everyone
run through an application pre release and verify the requirements from a list.
We called this FAT (factory acceptance testing) or ATP (acceptance test planning).

We would catch a lot of bugs and know what areas were still incomplete doing this method.
However, we would store the tests as excel sheets and send the results to the project
manager via Slack or we would use a Confluence sheet.

Both methods above were messy, so this web application is an attempt to replace those, and as
a more selfish reason, improve my skills by making a web app in Rust and HTMX.

## Features

- Simple (other options look like large project manage apps)
- Create and group tests
- View results of the tests and filter by tester and/or "test action"
- Add attachments to a test result
- Test cases can have attachments for making the test case clearer
- Manage users and roles

## Workflow

Tests can be grouped (example: group by app and then a test for each module)

Each test has a name, setup instructions to perform the test and a summary or description
of the test.

Tests contain actions, such as "click users -> manage users -> create user". Actions can
also have an attachment like a screenshot to further clarify.

Each action contains one or more cases. Example for the above action: "user appears in
database", "default fields filled in", etc.

A test run is when someone performs the actions and checks the test cases. Test runs
contain the tester's name (user name), date the test was started and any relevant notes on
the environment (browser, OS, app version, etc).

Each test run will produce a test case result for each test case. This will contain a
pass/fail mark, notes and attachments.

